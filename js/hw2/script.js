// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.

// 1. При завантаженні зображень чи відео, якщо новий прогресивний формат не підтримується браузером користувача,
// то можна відобразити альтернативний ресурс.
// 2. При відсутності доступу до ресурсу/файлу можна вивести альтернативу чи повідомлення про недоступність ресурсу/файлу.
// 3. При роботі зі сторонньою бібліотекою, якщо з якоїсь причини вона не підключилась або не підтримується,
// то можна виконати альтернативний код без її використання.

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70,
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70,
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70,
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40,
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    },
];

const root = document.getElementById("root");

const booksList = document.createElement("ul");

for (let book of books) {
    try {
        if (!book.author) {
            throw new Error(
                `Помилка: в об'єкті ${JSON.stringify(
                    book
                )} відсутня властивість 'author'`
            );
        }
        if (!book.name) {
            throw new Error(
                `Помилка: в об'єкті ${JSON.stringify(
                    book
                )} відсутня властивість 'name'`
            );
        }
        if (!book.price) {
            throw new Error(
                `Помилка: в об'єкті ${JSON.stringify(
                    book
                )} відсутня властивість 'price'`
            );
        }

        const li = document.createElement("li");
        li.innerText = `${book.author}: ${book.name} - ${book.price} грн`;

        booksList.append(li);
    } catch (error) {
        console.error(error);
    }
}

root.append(booksList);
