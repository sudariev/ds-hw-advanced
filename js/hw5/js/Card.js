export default class Card {
    constructor(name, email, title, text, id) {
        this.name = name;
        this.email = email;
        this.title = title;
        this.text = text;
        this.id = id;
    }
    createElement(elemType, classNames, text) {
        const element = document.createElement(elemType);
        if (text) element.textContent = text;
        element.classList.add(...classNames);
        return element;
    }
    renderHeader(container) {
        const header = this.createElement("div", ["card-header"]);
        header.insertAdjacentHTML(
            "afterbegin",
            `<a href=# class= 'card-name'> ${this.name} </a>
          <div class="btn-del"> <svg fill="#000000" xmlns:xlink="http://www.w3.org/1999/xlink" width="14px" height="14px" viewBox="0 0 469.404 469.404" xml:space="preserve"><path d="M310.4,235.083L459.88,85.527c12.545-12.546,12.545-32.972,0-45.671L429.433,9.409c-12.547-12.546-32.971-12.546-45.67,0 L234.282,158.967L85.642,10.327c-12.546-12.546-32.972-12.546-45.67,0L9.524,40.774c-12.546,12.546-12.546,32.972,0,45.671 l148.64,148.639L9.678,383.495c-12.546,12.546-12.546,32.971,0,45.67l30.447,30.447c12.546,12.546,32.972,12.546,45.67,0 l148.487-148.41l148.792,148.793c12.547,12.546,32.973,12.546,45.67,0l30.447-30.447c12.547-12.546,12.547-32.972,0-45.671 L310.4,235.083z"></path> </svg></div>  
          <div class="card-email"> ${this.email} </div>`
        );
        container.append(header);
        header.addEventListener("click", (e) => {
            const deleteTarget = e.target.closest(".btn-del");
            if (deleteTarget) {
                const delTwit = confirm("Delete this twit?");
                if (delTwit) {
                    this.card.remove();
                    fetch(
                        `https://ajax.test-danit.com/api/json/posts/${this.id}`,
                        {
                            method: "DELETE",
                        }
                    );
                }
            }
        });
    }
    renderText(container) {
        const text = this.createElement("div", ["card-content"]);
        text.insertAdjacentHTML(
            "afterbegin",
            `<div class="card-title"> ${this.title} </div>  <div class="card-text"> ${this.text} </div>`
        );
        container.append(text);
    }
    render() {
        this.card = this.createElement("div", ["card-container"]);
        document.querySelector(".wrapper").append(this.card);
        this.renderHeader(this.card);
        this.renderText(this.card);
        return this.card;
    }
}
