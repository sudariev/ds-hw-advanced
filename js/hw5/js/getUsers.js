export default async function getUsers() {
    const response = await fetch("https://ajax.test-danit.com/api/json/users");
    const users = await response.json();
    return users;
}
