export default async function getPosts() {
    const response = await fetch(`https://ajax.test-danit.com/api/json/posts`);
    const posts = await response.json();
    return posts;
}
