// 1) При прототипному наслідуванні обʼект отримує властивості та методи від свого прототипу.
// Якщо ми звертаємось до властивості або методу обʼекта і не знаходимо її (його),
// то пощук продовжиться далі в прототипі обʼєкта, а якщо і там не буде знайдено - в прототипі прототипа.

// 2) Виклик super() у конструкторі класу-нащадка використовується для того,
// щоб викликати конструктор класу-батька і передати йому аргументи. При використанні super() у конструкторі класу-нащадка,
// необхідно передати ті ж самі аргументи, які приймає конструктор батьківського класу.

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    set name(val) {
        this._name = val;
    }
    get name() {
        return this._name;
    }
    set age(val) {
        this._age = val;
    }
    get age() {
        return this._age;
    }
    set salary(val) {
        this._salary = val;
    }
    get salary() {
        return this._salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    set salary(val) {
        this._salary = val * 3;
    }
    get salary() {
        return this._salary;
    }
}

const programmer1 = new Programmer("Stepan", 25, 2000, "Ukrainian");
const programmer2 = new Programmer("Ben", 19, 1300, "American");
const programmer3 = new Programmer("William", 23, 600, "Australian");
console.log(programmer1);
console.log(programmer2);
console.log(programmer3);
