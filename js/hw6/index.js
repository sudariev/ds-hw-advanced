// Асинхронність в JS - це можливість виконання коду не послідовно, а в заданому бажаному порядку, що може зекономити обчислювальні ресурси або виконувати код більш ефективно, не очікуючи, доки слідуюча за порядком частина коду не буде виконана і результат повернений. Асинхронність корисна, наприклад, якщо ми працюємо з віддаленим сервером і не хочемо чекати обробки і виконання запиту, а виконувати паралельно якусь іншу частину коду...

const btn = document.querySelector(".btn");

btn.addEventListener("click", async (e) => {
    const responseIP = await fetch("https://api.ipify.org/?format=json");
    const resultIP = await responseIP.json();
    const responseAddress = await fetch(
        `http://ip-api.com/json/${resultIP.ip}`
    );
    const resultAddress = await responseAddress.json();
    showAddress(resultAddress);
});

function showAddress(place) {
    const div = document.createElement("div");
    div.style = `position: absolute;
                left:45%;
                top: 55%;`;
    document.body.append(div);
    div.insertAdjacentHTML(
        "afterbegin",
        `<div>Country: ${place.country}</div>
        <div>City: ${place.city}</div>
        <div>Country code: ${place.countryCode}</div>
        <div>Region: ${place.region}</div>
        <div>Region name: ${place.regionName}</div>
        <div>Timezone: ${place.timezone}</div>`
    );
}
