// AJAX забезпечує взаємодію з сервером без перезавантаження сторінки. Він дозволяє виконувати запити до сервера асинхронно, без очікування відповіді перед відображенням результатів на сторінці і без очікування завершення запиту на сервері.

const filmsList = document.querySelector(".filmsList");

const showFilmsChars = () => {
    fetch("https://ajax.test-danit.com/api/swapi/films")
        .then((response) => response.json())
        .then((arrFilms) => {
            arrFilms.forEach((film) => {
                Promise.all(getActors(film)).then((val) => {
                    showFilmsInfo(film, val);
                });
            });
        });
};

const showFilmsInfo = (film, val) => {
    const episodeId = createDOMElement("li", "EPISODE " + film.episodeId);
    const name = createDOMElement("p", film.name.toUpperCase());
    const openingCrawl = createDOMElement("p", "SUMMARY: " + film.openingCrawl);
    const actors = createDOMElement("p", "ACTORS: ");
    val.forEach((el) => {
        actors.textContent += el.name + " ";
    });
    filmsList.append(episodeId);
    episodeId.append(name, actors, openingCrawl);
};

const getActors = (film) => {
    return film.characters.map(
        (id) => (response = fetch(id).then((response) => response.json()))
    );
};

const createDOMElement = (tag, text) => {
    const elem = document.createElement(tag);
    elem.textContent = text;
    return elem;
};

showFilmsChars();
